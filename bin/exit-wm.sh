#!/bin/bash
# Name:     exit-wm
# About:    simple "logout" gui for wms
# Author:   grimi
# License:  GNU GPL v3
# Required: grep,pkill,xprop
# Required: zenity or Xdialog or xterm+dialog
# Required: systemd or elogind or consolekit+dbus or sudo+shutdown


case ${LANG%.*} in
  "pl_PL")
    USSD="  > Nie zapomnij dodać komendy shutdown do /etc/sudoers <"
    ERXD=">>> Uwaga: zainstaluj zenity albo xdialog albo dialog+xterm. <<<"
    MESG="Wybierz właściwą opcję:"
    MOPT="Opcja"
    MSEL="Wybór"
    LOGO="Wyloguj"
    REST="Uruchom ponownie"
    HALT="Wyłącz"
  ;;
  *)
    USSD="  > Don't forget add shutdown command to /etc/sudoers <"
    ERXD=">>> Warning: install zenity or xdialog or dialog+xterm. <<<"
    MESG="Select correct option:"
    MOPT="Option"
    MSEL="Select"
    LOGO="Logout"
    REST="Reboot"
    HALT="Halt"
  ;;
esac


ZENDIM="--width 320 --height 230"

declare -A WMTAB=([openbox]="openbox --exit" [i3]="i3-msg exit" [jwm]="jwm -exit")

NAME="${0##*/}"

if [[ $1 == "-h" || $1 == "--help" ]]; then
  echo "$NAME: [test] [-x] [-d]"
  exit 1
fi

if [[ $1 == "test" ]]; then
  shift; TEST=1
fi

if [[ $1 != "-x" ]] && [[ $1 != "-d" ]]; then
  ZENITY="$(command -v zenity)"
fi

if [[ -z $ZENITY || $1 == "-x" ]] && [[ $1 != "-d" ]]; then
  XDIALOG="$(command -v Xdialog)"
fi
if [[ -z $XDIALOG || $1 == "-d" ]]; then
  DIALOG="$(command -v dialog)"
  XTERM="$(command -v xterm)"
  if [[ -z $DIALOG || -z $XTERM ]] && [[ -z $ZENITY ]]; then
    echo "$ERXD" ; exit 1
  fi
fi

findwm() {
   local rootwinid="$(xprop -root _NET_SUPPORTING_WM_CHECK)"
   rootwinid="${rootwinid##* }"
   local wmname="$(xprop -id $rootwinid 8s _NET_WM_NAME)"
   wmname="${wmname#*\"}";  wmname="${wmname%\"*}"
   echo "${wmname,,}"
}


exitwm() {
  local wmname="$(findwm)" cmd
  if [[ $TEST -eq 1 ]] ; then
    echo ">>> in exitwm:"
  fi
  if [[ $wmname ]]; then
    cmd="${WMTAB["$wmname"]}"
    if [[ $cmd ]]; then
      if [[ $TEST -eq 1 ]]; then
        echo "$cmd"
      else
        $cmd
      fi
    else
      if [[ $TEST -eq 1 ]]; then
        echo "pkill -i -SIGHUP $wmname || sleep 1 && pkill -i -SIGKILL $wmname"
      else
        pkill -i -SIGHUP $wmname || sleep 1 && pkill -i -SIGKILL $wmname
      fi
    fi
  fi
}


haltsys() {
  local ahalt
  if [[ -n $(command -v pkaction) ]]; then
    ahalt="$(pkaction|grep -m1 -E "power-off|stop")"
  fi
  if [[ $ahalt == "org.freedesktop.login1.power-off" ]]; then
    if [[ -n $(command -v systemctl) ]]; then
      if [[ $TEST -eq 1 ]]; then
        echo "systemctl poweroff"
      else
        systemctl poweroff
      fi
    elif [[ -n $(command -v loginctl) ]]; then
      if [[ $TEST -eq 1 ]]; then
        echo "loginctl poweroff"
      else
        loginctl poweroff
      fi
    fi
  elif [[ $ahalt == "org.freedesktop.consolekit.system.stop" ]]; then
    if [[ $TEST -eq 1 ]]; then
      echo "dbus-send --system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop"
    else
      dbus-send --system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop
    fi
  else
    if [[ $TEST -eq 1 ]]; then
      echo "sudo shutdown -h now"
    else
      sudo -n shutdown
      if [[ $? -ne 0 ]]; then
        sudo -n shutdown -h now
      fi
    fi
  fi
}


rebootsys() {
  local areboot
  if [[ -n $(command -v pkaction) ]]; then
    areboot="$(pkaction|grep -m1 -E 'reboot|restart')"
  fi
  if [[ $areboot == "org.freedesktop.login1.reboot" ]]; then
    if [[ -n $(command -v systemctl) ]]; then
      if [[ $TEST -eq 1 ]]; then
        echo "systemctl reboot"
      else
        systemctl reboot
      fi
    elif [[ -n $(command -v loginctl) ]]; then
      if [[ $TEST -eq 1 ]]; then
        echo "loginctl reboot"
      else
        loginctl reboot
      fi
    fi
  elif [[ $areboot == "org.freedesktop.consolekit.system.restart" ]]; then
    if [[ $TEST -eq 1 ]]; then
      echo "dbus-send --system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart"
    else
      dbus-send --system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart
    fi
  else
    if [[ $TEST -eq 1 ]]; then
      echo "sudo shutdown -r now"
    else
      sudo -n shutdown
      if [[ $? -ne 0 ]]; then
        sudo -n shutdown -r now
      fi
    fi
  fi
}


if [[ -n $ZENITY ]]; then
  OPTIO="$(zenity --title "$NAME" --text "$MESG" $ZENDIM --list --radiolist --column "$MSEL" --column "$MOPT" TRUE "$LOGO" FALSE "$REST" FALSE "$HALT")"
elif [[ -n $XDIALOG ]]; then
  OPTIO="$(Xdialog --stdout --no-tags --title "$NAME" --radiolist "$MESG" 14 43 9  "$LOGO" "$LOGO" ON "$REST" "$REST" OFF "$HALT" "$HALT" OFF)"
else
  export MESG LOGO REST HALT
  xterm -T "$NAME" -g 43x10 -e 'echo $(dialog --no-shadow --stdout --radiolist "$MESG" 10 43 9 "$LOGO" "" ON "$REST" "" OFF "$HALT" "" OFF) >/dev/shm/exit-wm.cmd'
  OPTIO="$(</dev/shm/exit-wm.cmd)"
  rm -f /dev/shm/exit-wm.cmd
fi

cd /

case "$OPTIO" in
  "$LOGO")  exitwm ;;
  "$REST")  rebootsys ;;
  "$HALT")  haltsys ;;
esac


# vim:set ts=2 sw=2 sts=2 et:

